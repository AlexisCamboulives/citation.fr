import { Application } from "express";
import HomeController from "./controllers/HomeController";
import CitationController from "./controllers/CitationController";
import AuteurController from "./controllers/AuteurController";
import BackOfficeController from "./controllers/BackOfficeController";
import CitationByAuteurController from "./controllers/CitationByAuteurController";

export default function route(app: Application)
{
    /** Static pages **/
    app.get('/', (req, res) =>
    {
        HomeController.index(req, res);
    });
    app.get('/citation/:id', (req, res) =>
    {
        CitationController.citations(req, res);
    });
    app.get('/author', (req, res) =>
    {
        AuteurController.authors(req, res);
    });
    app.get('/backoffice', (req, res) =>
    {
        BackOfficeController.backOffice(req, res);
    });
    app.get('/citations-by-author/:id_author', (req, res) =>
    {
        CitationByAuteurController.citationsByAuthor(req, res);
    });
    app.get('/author-create', (req, res) => {
        BackOfficeController.showFormCreateAuthor(req, res);
    });
    app.get('/citation-create', (req, res) =>{
        BackOfficeController.showFormCreateCitation(req, res);
    });
    app.post('/citation-create', (req, res) =>
    {
        BackOfficeController.createCitation(req, res);
    });
    app.post('/author-create', (req, res) => 
    {
        BackOfficeController.createAuthor(req, res);
    });
    app.get('/citation-update/:id_citation', (req, res) =>
    {
        BackOfficeController.showFormCitationUpdate(req, res)
    });

    app.post('/citation-update/:id_citation', (req, res) =>
    {
        BackOfficeController.updateCitation(req, res)
    });
    app.get('/citation-delete/:id_citation', (req, res) =>
    {
        BackOfficeController.deleteCitation(req, res)
    });
    app.get('/author-update/:id_author', (req, res) =>
    {
        BackOfficeController.showFormAuthorUpdate(req, res)
    });
    app.post('/author-update/:id_author', (req, res) =>
    {
        BackOfficeController.updateAuthor(req, res)
    });
    app.get('/author-delete/:id_author', (req, res) =>
    {
        BackOfficeController.deleteAuthor(req, res)
    });
}
