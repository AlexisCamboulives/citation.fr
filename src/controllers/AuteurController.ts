import { Request, Response } from "express-serve-static-core";
import { db } from "../server";

export default class HomeController
{
    static authors(req: Request, res: Response): void
    {
        //Sélection de tout les auteurs aillant déjà publié
        let allAuthors = db.prepare('SELECT * FROM author WHERE publish = 1').all()
        res.render('pages/author', {
            title: 'Les auteurs',
            authors: allAuthors,
        });
    }

}