import { Request, Response } from "express-serve-static-core";
import { db } from "../server"

export default class HomeController
{
    static citations(req: Request, res: Response): void
    {
        
        const limit = 10;
        let btnLink = +req.params.id;
        let offset = ((limit * btnLink) -10);
        console.log(req.params.id)
        let allCitations = db.prepare('SELECT * FROM citation LIMIT 10 OFFSET ?').all(offset);
        
        res.render('pages/citation', {
            title: 'Les citations',
            citations: allCitations,
            btnNum: btnLink,
        });
    }

}