import { Request, Response } from "express-serve-static-core";

import { db } from "../server";

export default class HomeController
{
    /**
     * Affiche la page du back office avec les citations et les auteurs;
     * @param req 
     * @param res 
     */ 
    static backOffice(req: Request, res: Response): void
    {
        let allCitation = db.prepare('SELECT * FROM citation').all();
        let allAuthor = db.prepare('SELECT * FROM author').all();
        res.render('pages/backoffice', {
            title: 'Le back-office',
            citations: allCitation,
            authors: allAuthor,
        });
    }
    /**
     * Affiche le formulaire de création des citations
     * @param req 
     * @param res 
     */
    static showFormCreateCitation(req: Request, res: Response): void
    {
        let selectAuthorByName = db.prepare('SELECT * FROM author').all();
        res.render('pages/citation-create',{
            title: 'Créer une citation',
            authors: selectAuthorByName,
        });
    }
    /**
     * Affiche le formulaire de création des auteurs
     * @param req 
     * @param res 
     */
    static showFormCreateAuthor(req: Request, res: Response): void
    {
        
        res.render('pages/author-create', {
            title: 'Créer un author',
        });
    }
    /**
     * Mise en db des values du formulaire de création des citations
     * @param req 
     * @param res 
     */
    static createCitation(req: Request, res: Response): void
    {
        //récupération des données du formulaire
        let submitAuthor = req.body.author;
        let selectAuthor = db.prepare('SELECT * FROM author WHERE username=?').get(submitAuthor);
        //Insert des données en db
        let addCitation = db.prepare('INSERT INTO citation ("content", "author_id_author") VALUES (?, ?)').run(req.body.citation, selectAuthor.id_author);
        //Passage de la propriété publish des auteurs en true
        let addTrueAsPublish = db.prepare('UPDATE author SET publish=1 WHERE username=?').run(submitAuthor);
        res.redirect('/backoffice');
    }
    /**
     * Mise en db des values du formulaire de création des auteurs
     * @param req 
     * @param res 
     */
    static createAuthor(req: Request, res: Response): void
    {
        //Récupération des données du formulaire
        let submitUsername = req.body.username;
        //Insertion des données dans la db
        let addAuthor = db.prepare("INSERT INTO author ('username') VALUES (?)").run(submitUsername);
        res.redirect('/backoffice');
    }
    /**
     * Affiche du formulaire d'update des citations
     * @param req 
     * @param res 
     */
    static showFormCitationUpdate(req: Request, res: Response)
    {

        //Récupération de la citation pour afficher les données dans le formulaire
        let selectCitationForUpdate = db.prepare('SELECT * FROM citation WHERE id_citation = ?').get(req.params.id_citation);
        //Récupération de l'auteur de la citation pour affichage dans le formulaire
        let authorCitation = db.prepare('SELECT * FROM author WHERE id_author = ?').get(selectCitationForUpdate.author_id_author);

        let selectAuthorByName = db.prepare('SELECT * FROM author').all();

        res.render('pages/citation-update', {
            citations: selectCitationForUpdate,
            authors: authorCitation,
            selectAuthor: selectAuthorByName,
            title: 'Modifier une citation',
        });
    }
    /**
     * Mise en db des values du formulaire d'update des citations
     * @param req 
     * @param res 
     */
    static updateCitation(req: Request, res: Response)
    {
        //récupération des données du formulaire
        let updateCitationAuthor = db.prepare('SELECT * FROM author WHERE username = ?').get(req.body.author);
        //Modification des données dans la db
        let updateCitationContent = db.prepare('UPDATE citation SET content = ?, author_id_author = ? WHERE id_citation = ?').run(req.body.citation,updateCitationAuthor.id_author, req.params.id_citation);
        res.redirect('/backoffice');
    }
    /**
     * Suppression des données de la db des citations  
     * @param req 
     * @param res 
     */
    static deleteCitation(req: Request, res: Response)
    {
        let deleteCitation = db.prepare('DELETE FROM citation WHERE id_citation = ? ').run(req.params.id_citation);
        res.redirect('/backoffice');
    }
    /**
     * Affichage formulaire de modiffication d'auteur
     * @param req 
     * @param res 
     */
    static showFormAuthorUpdate(req: Request, res: Response)
    {

        //Récupération de l'author pour afficher les données dans le formulaire
        let selectAuthorForUpdate = db.prepare('SELECT * FROM author WHERE id_author = ?').get(req.params.id_author);

        res.render('pages/author-update', {
            authors: selectAuthorForUpdate,
            title: 'Modifier une author',
        });
    }
    /**
     * Mise en db des values du formulaire d'update des auteurs
     * @param req 
     * @param res 
     */
    static updateAuthor(req: Request, res: Response)
    {
        //Modification des données dans la db
        let updateAuthorContent = db.prepare('UPDATE author SET username = ? WHERE id_author = ?').run(req.body.author, req.params.id_author);
        res.redirect('/backoffice');
    }
    /**
     * Suppression des données de la db des auteurs  
     * @param req 
     * @param res 
     */
    static deleteAuthor(req: Request, res: Response)
    {
        const deleteAllCitationAuthor = db.prepare('DELETE FROM citation WHERE author_id_author = ?').run(req.params.id_author);
        const deleteAuthor = db.prepare('DELETE FROM author WHERE id_author = ?').run(req.params.id_author);
        res.redirect('/backoffice');
    }
}