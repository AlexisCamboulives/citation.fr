import { Request, Response } from "express-serve-static-core";
import { db } from "../server"

export default class HomeController
{
    static citationsByAuthor(req: Request, res: Response): void
    {
        const selectAuthor = db.prepare('SELECT * FROM author WHERE id_author = ?').get(req.params.id_author);
        const selectAuthorCitation = db.prepare('SELECT content FROM citation WHERE author_id_author = ?').all(selectAuthor.id_author);
        console.log(selectAuthorCitation)
        res.render('pages/citations-by-author', {
            title: "Les citations de ",
            authors: selectAuthor,
            citations: selectAuthorCitation,
        });
    }

}