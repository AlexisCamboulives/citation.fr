import { Request, Response } from "express-serve-static-core";
import { db } from "../server";

export default class HomeController {

    static index(req: Request, res: Response): void {

        //Selection de la dernière citation publié
        const selectLastCitation = db.prepare('SELECT * FROM citation ORDER BY date DESC LIMIT 1').all();
        const selectCitationContent = selectLastCitation.find(lastContent => lastContent.content);
        //Selection de l'auteur de la dernière citation
        const selectIdAuthor = selectLastCitation.find(author_id => author_id.author_id_author);
        const selectLastCitationAuthor = db.prepare('SELECT * FROM author WHERE id_author = ?').get(selectIdAuthor.author_id_author);

        res.render('pages/index', {
            title: 'Citation.fr',
            lastCitation: selectCitationContent,
            lastCitationAuthor: selectLastCitationAuthor,
        });
    }
}