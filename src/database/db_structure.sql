-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Alexis
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2022-02-21 11:02
-- Created:       2022-02-21 10:54
PRAGMA foreign_keys = OFF;

-- Schema: mydb
-- ATTACH "mydb.sdb" AS "mydb";
BEGIN;
CREATE TABLE "author"(
  "id_author" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "username" VARCHAR(45),
  "publish" BOOLEAN NOT NULL DEFAULT FALSE
);
CREATE TABLE "citation"(
  "id_citation" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "content" LONG VARCHAR,
  "author_id_author" INTEGER NOT NULL,
  "date" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT "fk_citation_author"
    FOREIGN KEY("author_id_author")
    REFERENCES "author"("id_author")
);
CREATE INDEX "citation.fk_citation_author_idx" ON "citation" ("author_id_author");
COMMIT;
